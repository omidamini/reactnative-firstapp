create project: expo init pleazia 
install webview : expo install react-native-webview 
Building Standalone Apps: https://docs.expo.dev/classic/building-standalone-apps/
install hooks: npm install @react-native-community/hooks
run "npm audit fix" command to fix vulnerabilities
install geo-location: expo install expo-location 
(if expo doesn't work try to use "yarn add expo-location@~14.0.1")
to install audio-record: expo install expo-av   