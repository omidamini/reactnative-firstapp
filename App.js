
import React, { useState } from 'react';
import { SafeAreaView, Pressable, FlatList, TextInput, StyleSheet, Text, View, ImageBackground } from "react-native";
const sampleGoal = [
  "Faire les courses",
  "Aller à la salle de sport 3 fois par semaine",
  "Monter à plus de 5000m d altitude",
  "Acheter mon premier appartement",
  "Perdre 5 kgs",
  "Gagner en productivité",
  "Apprendre un nouveau langage",
  "Faire une mission en freelance",
  "Organiser un meetup autour de la tech",
  "Faire un triathlon",
];
const imgBackground = require('./assets/lyon-1.jpg')
export default function App() 
{
  const itemSeparator = () => {
    return <View style={{ height: 1, backgroundColor: "grey",marginHorizontal:10}} />;
    };
  const [sampleGoals, setSamplegoal] = useState(sampleGoal);
  const [text, setText] = useState('');
  const handleClick = (value) =>
  {
    setSamplegoal([...sampleGoals, value]);
  }
  const removeSamplegoal =(value) =>
  {
    sampleGoals.splice(sampleGoals.indexOf(value), 1);
    setSamplegoal([...sampleGoals]);
  }
  return (
  <SafeAreaView style={styles.container}>
  <ImageBackground source={imgBackground} style={styles.imageBackground} >
    <View>
      <Text style={{ fontSize: 30, textAlign: "center",marginTop:20,textDecorationLine: 'none', color:"red" }}>
        Open up <Text style={{fontWeight:'bold'}}>App.js</Text> to start working on your app!
      </Text>
    </View>
    <FlatList 
      data={sampleGoals}
      renderItem={({ item }) => 
      <View onChange={(sampleGoals) => setSamplegoal(sampleGoals)}>
      <Text style={styles.item}>{item}</Text>
      <Pressable  onPress={() => removeSamplegoal(item)}>
        <Text style = {{color :'red', maxWidth:500, textAlign: "right", marginRight: 30, fontWeight: 'bold',}}>X</Text>
      </Pressable>
      </View>}
      ItemSeparatorComponent={itemSeparator}
      keyExtractor={(item, index) => index}
    />
    <View>
      <TextInput
        style={styles.input}
        onChangeText={(text) => setText(text)}
        value = {text}
        placeholder="écrivez votre texte"
        //keyboardType="numeric"
      />
      <Pressable style={styles.button} onPress={() => handleClick(text)}>
        <Text style={styles.buttonText}>Add</Text>
      </Pressable>
    </View> 
  </ImageBackground>
  </SafeAreaView>
  );
 }
 
const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: 5,
    fontSize: 30,
  },
  imageBackground:{
    flex:1, 
    resizeMode:"cover", 
    justifyContent:"center"
  },
  item: {
    padding: 10,
    marginTop: 2,
    fontSize: 20,
    maxWidth :500,
  },
  input: {
    height: 40,
    margin: 0,
    borderWidth: 1,
    padding: 10,
    borderColor:'blue'
  },
  button: {
    alignItems: 'center',
    justifyContent: 'center',
    paddingVertical: 12,
    paddingHorizontal: 32,
    borderRadius: 0,
    elevation: 3,
    backgroundColor: 'blue',
  },
  buttonText: {
    fontSize: 16,
    lineHeight: 21,
    fontWeight: 'bold',
    letterSpacing: 0.25,
    color: 'white',
  }
});  
